
window.onload = function() {

    function renderCircle(container, diameter) {
        const circle = document.createElement('span');
        circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 80%, 50%)`;
        circle.style.width = diameter + 'px';
        circle.style.height = diameter + 'px';
        circle.style.display = 'inline-block';
        circle.style.borderRadius = '50%';
        container.append(circle);

        circle.onclick = function() {
            circle.remove();
        }
    }

    document.getElementById('circleDrawStart').onclick = function () {
        // видаляемо стару кнопку
        document.body.innerHTML = '';
        // створюемо инпут з якого будемо брати діамтр
        const input = document.createElement('input');
        document.body.append(input);
        // створюемо инпут для початки обробки
        const button = document.createElement('button');
        button.innerHTML = 'Намалювати';
        document.body.append(button);
        // створюемо див контейнер для кіл
        const container = document.createElement('div');
        document.body.append(container);

        // при натисненні на кнопку починаемо функцію-побудови
        button.onclick = function() {
            //вносимо параметр з інпута до змінної
            const diameter = input.value;
            //створюемо змінну для довжині контейнера
            const containerWidth = diameter * 10;
            //добавляемо в стиль довжину 
            container.style.width = containerWidth + 'px';
            //допомагае відновити Дів при повторному оновленю сторінки
            container.innerHTML = '';
            //створюемо кола
            for(let i = 0; i < 100; i++) {
                renderCircle(container, diameter);
            }
        }
    
    };

    
}
